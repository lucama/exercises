package util_thimoty.test;


import util_thimoty.cad.model.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	@Test
	public void testCircle() {
		
		int x = 0;
		int y = 0;
		Point origin = new Point(x, y);
		double radius = 2.0;
		
		Circle circle = new Circle(origin, radius);
		double circleArea = circle.calcArea();
		
		assertEquals("The area of the circle is correct", circleArea, 2*2*Math.PI,0);
		
	}

}
