package util_thimoty.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CircleTest.class, 
	SquareTest.class,
	CanvasTest.class,
	RectangleTest.class
})

public class AllTests {

}
