package util_thimoty.test;


import util_thimoty.cad.model.Point;
import util_thimoty.cad.model.Rectangle;

import static org.junit.Assert.*;

import org.junit.Test;

public class RectangleTest {

	@Test
	public void testRectangle() {
		
		Rectangle rect = new Rectangle(new Point(0,0), 2.0, 3.0);
		double rectArea = rect.calcArea();
		
		assertEquals("The area of the rectangle is correct", rectArea, 6.0, 0);
		
		
	}
	
}
