package util_thimoty.test;


import static org.junit.Assert.*;

import util_thimoty.cad.model.*;

import org.junit.Test;

public class SquareTest {

	/**
	 * I test the creation of a square
	 */
	@Test
	public void testSquareCreate() {
		
		// Simple test case
		int x = 0;
		int y = 0;
		double side = 2.0; 
		
		Point origin = new Point(x, y);
		Square square = new Square(origin, side);
		
		double squareArea = square.calcArea();
		
		assertEquals("The area of the square is correct", squareArea, 4.0,0);
		
		
	}

}
