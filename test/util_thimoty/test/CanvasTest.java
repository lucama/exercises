package util_thimoty.test;

import static org.junit.Assert.*;


import util_thimoty.cad.model.*;
import util_thimoty.cad.*;



import org.junit.Test;

public class CanvasTest {

	@Test
	public void testCanvas() {
		
		Canvas canvas = new Canvas();
		
		Point origin = new Point(0, 0);
		double side = 2.0;
		double radius = 2.0;
		
		Square square = new Square(origin, side);
		Circle circle = new Circle(origin, radius);
		Rectangle rect = new Rectangle(origin, 3.0, 2.0);
		
		canvas.add(square);
		canvas.add(circle);
		canvas.add(rect);
		
		double totalArea = canvas.calcTotalArea();
		
		assertEquals("The total area of the canvas is correct", totalArea, 4+2*2*Math.PI+6, 0);
		
		
	}

}
