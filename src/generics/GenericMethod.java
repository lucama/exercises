package generics;

public class GenericMethod
{

	public static void main(String[] args)
	{
		GenericMethod genMeth = new GenericMethod();
		A a = new A();
		B b = new B();
		genMeth.genericMethod(a);
		genMeth.genericMethod(b);

	}
	
	
	public A genericMethod( A par){
		return par;
	}

}
