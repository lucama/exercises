package java8.maps;
import java.util.HashMap;
import java.util.Map;

public class MapEx
{

	public static void main(String[] args)
	{
		Map<String,Object> map = new HashMap<>();
		map.put("primo", "aaa");
		map.put("secondo", 2);
		//
		System.out.println(map);
	}

}
