package java8.maps;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.util.AbstractMap.SimpleEntry;
public class MapExample
{

    public static void main(String[] args)
    {
       
    }
    
    
    protected static Map<Integer, String> stdJava8() {

        return Collections.unmodifiableMap(Stream.of(
                new SimpleEntry<>(0, "zero"),
                new SimpleEntry<>(1, "one"))
                .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));
    }
   
}
