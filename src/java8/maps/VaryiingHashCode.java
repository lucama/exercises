package java8.maps;

import java.util.HashMap;
import java.util.Map;

public class VaryiingHashCode
{

	public static void main(String[] args)
	{
		User user = (new VaryiingHashCode()). new User ("aaa");
		Map<User, String> usersMap = new HashMap<User, String>();
		usersMap.put(user, user.getName());
		
		System.out.println(usersMap.get(user)); //OK ritrovo l'elemento
		
		user.setName("abc");  //cambio hashcode dell'oggetto --> nella mappa rimane hashcode vecchio
		
		System.out.println(usersMap.get(user)); //Non trovo pi� la chiave poich� � modificato l'hashCode
	}

	
	private class User {
		private String name ="";

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public User(String name)
		{
			super();
			this.name = name;
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			User other = (User) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (name == null)
			{
				if (other.name != null)
					return false;
			}
			else
				if (!name.equals(other.name))
					return false;
			return true;
		}

		private VaryiingHashCode getOuterType()
		{
			return VaryiingHashCode.this;
		}
	}
}
