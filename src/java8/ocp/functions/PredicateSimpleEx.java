package java8.ocp.functions;

import java.util.function.Predicate;

public class PredicateSimpleEx
{
/**
 * Predicate
 * 
 * Parameter type: T
 * Return type: boolean
    Description: Represents a predicate (boolean-valued function) of one argument.
 * @param args
 */
	public static void main(String[] args)
	{
		Predicate<String> i  = (s)-> s.length() > 5;	  
	    System.out.println(i.test("java2s.com "));

	}

}
