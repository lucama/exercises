package java8.ocp.functions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/*
 * java.util.function.Consumer
	Parameter type: T
	Return type: void
	Description: Represents an operation that accepts a single input argument and returns no result.
 */

public class ConsumerEx<T>
{

	public static void main(String[] args)
	{
		
		List<Integer> myList = new ArrayList<Integer>();
		for(int i=0; i<10; i++) myList.add(i);
		
		//traversing using Iterator
//		Iterator<Integer> it = myList.iterator();
//		while(it.hasNext()){
//			Integer i = it.next();
//			System.out.println("Iterator Value::"+i);
//		}
		
		//traversing through forEach method of Iterable with anonymous class
		myList.forEach(new Consumer<Integer>() {

			public void accept(Integer t) {
				System.out.println("forEach anonymous class Value::"+t);
			}

		});
		
		//traversing with Consumer interface implementation
		ConsumerEx<Integer>.ConsumerLog action = new ConsumerEx<Integer>(). new ConsumerLog();
		myList.forEach(action);
		

	}

	
	//Consumer that log element
	class ConsumerLog implements Consumer<T>{

		public void accept(T t) {
			System.out.println("Consumer impl Value::"+t);
		}


	}
	
}
