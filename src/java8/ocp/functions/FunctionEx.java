package java8.ocp.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * * 
 * mappare una lista in una altra lista
 * @author 
 *
 */
public class FunctionEx
{

	private int personId;
    public int getPersonId()
	{
		return personId;
	}

	public String getJobDescription()
	{
		return jobDescription;
	}

	private String jobDescription;

    public FunctionEx(int personId, String jobDescription) {
        super();
        this.personId = personId;
        this.jobDescription = jobDescription;
    }
	
	/*
	 *  Parameter type: T
		Return type: R
		Description: Represents a function that accepts one argument and produces a result.
	 */
	public static void main(String[] args)
	{
	
		List<FunctionEx> persons = Arrays.asList(new FunctionEx(1, "Husband"),
	            new FunctionEx(2, "Dad"), new FunctionEx(3, "Software engineer"),
	            new FunctionEx(4, "Adjunct instructor"));

		List<String> jobs = persons.stream().map(mapToJob)
	            .collect(Collectors.toList());

	   System.out.println(jobs);

	   
		
	}
	
	static Function<FunctionEx,String> mapToJob = new Function<FunctionEx,String>() {
	    public String apply(FunctionEx person) {
	         return person.getJobDescription();
	    }
	};
	
		 
	 
}
