package java8.ocp.functions;

import java.util.function.Predicate;

interface AmountValidator{
	public boolean checkAmount(double value); 
} 

public class PredicateEx1 {
//	public void updateBalance(double bal ){  
//		boolean isOK = new AmountValidator(){ 
//		public boolean checkAmount(double val){ 
//			return val >= 0.0;
//			} 
//	}.checkAmount(bal);
	//other irrelevant code     
	public void updateBalance(double bal ){ 
		Predicate<Double> p = val -> val>=0.0;
		//use lamba expression to create a Predicate       
		boolean isOK = p.test(bal);
		}

	
}




