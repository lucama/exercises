package java8.ocp.dateTime;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

public class Java8DateTime
{

	public static void main(String[] args){
		Duration d = Duration.ofMillis(1100); 
		System.out.println(d);
		d = Duration.ofSeconds(61); 
		System.out.println(d);
		
		
		Instant epoch = Instant.EPOCH;
		
		// get clock with desired time-zone
		Clock clock = Clock.system(ZoneId.of("America/Chicago"));

		Instant now = Instant.now(clock);
		System.out.println(now);
	}
	
	
}
