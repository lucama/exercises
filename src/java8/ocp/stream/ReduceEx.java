package java8.ocp.stream;

import java.util.Arrays;
import java.util.List;

public class ReduceEx
{

	public static void main(String[] args)
	{
		List<String> vals = Arrays.asList("a", "b"); 
		String join = vals.stream()
				.reduce("_", (a, b)->a.concat(b));
		System.out.println(join);

	}

}
