package java8.ocp.stream;

import java.util.Arrays;
import java.util.List;

public class MaxEx
{

	public static void main(String[] args)
	{
		//Which of the floowing  statements will print 9?
		
		List<Integer> ls = Arrays.asList(3,4,6,9,2,5,7); 
		
		System.out.println(ls.stream().reduce(Integer.MIN_VALUE, (a, b)->a>b?a:b)); //OK
		
		System.out.println(ls.stream().max(Integer::max).get()); //KO 
		
		System.out.println(ls.stream().max(Integer::compare).get()); //OK
		
		System.out.println(ls.stream().max((a, b)->a>b?a:b)); //4 

	}

}
