package java8.ocp.stream.parallel;

import java.util.Arrays;
import java.util.List;

public class ParallerStrEx
{

	public static void main(String[] args)
	{
		List<String> vals = Arrays.asList("a", "b"); 
		
		String joinPar = vals.parallelStream()
				.reduce("_", (a, b)->a.concat(b));
		System.out.println("join parallel:"+ joinPar);
		String join = vals.stream()
				.reduce("_", (a, b)->a.concat(b));
		System.out.println("join :"+ join);

	}

}
