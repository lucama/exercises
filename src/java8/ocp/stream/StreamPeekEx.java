package java8.ocp.stream;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamPeekEx
{

	public static void main(String[] args)
	{
		//uso peek per debuggare
		Stream<String> words = Stream.of("lower", "case", "text");
		List<String> list = words
		    .peek(s -> System.out.println(s))
		    .map(s -> s.toUpperCase())
		    .collect(Collectors.toList());
		System.out.println(list);

	}

}
