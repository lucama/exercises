package java8.ocp.optional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OptionalMap
{
	private static Map<String, Optional<String>> map = new HashMap<>();
	
	public static void main(String[] args)
	{
		Optional<String> emptyMapVal = map.get("prova");
		if(emptyMapVal.isPresent()){
			System.out.println(emptyMapVal.orElse("prova key not found"));
		}	
	}

}
