package java8.ocp.optional;

import java.util.Optional;

public class OptionalEx
{

	public static void main(String[] args)
	{
		 
		Data data1 = null;
		Data data2 = (new OptionalEx()). new Data("aaa",2);
		printStr(data1);
		printStr(data2);
		
	}
	
	
	private static void printStr (Data data){
		
		
		//per capire codice esploso 
		Optional<Data> optionalData = Optional.ofNullable(data);
		Optional<String> optionalMapped = optionalData.map(p -> p.toString());// se il valore � presente lo mappo in un optional del tipo giusto altrimenti optionale empty
		String orElse = optionalMapped.orElse("ko"); //se optional � vuoto restituisc eil valore alternativo
		System.out.println(optionalMapped);
		System.out.println(orElse);
		
		Optional<Integer> optionalMapped2 = optionalData.map(p -> p.getData2());// se il valore � presente lo mappo in un optional del tipo giusto altrimenti optionale empty
		Integer orElse2 = optionalMapped2.orElse(0); //se optional � vuoto restituisc eil valore alternativo
		System.out.println(optionalMapped2);
		System.out.println(orElse2);
		
//		System.out.println(Optional.ofNullable(data)  
//        .map(p -> p.toString())
//        .orElse("nullDAta"));
	}

	private class Data {
		private String data1;
		@Override
		public String toString()
		{
			return "Data [data1=" + data1 + ", data2=" + data2 + "]";
		}
		private Integer data2;
		public Integer getData2()
		{
			return data2;
		}
		public Data(String data1, Integer data2)
		{
			super();
			this.data1 = data1;
			this.data2 = data2;
		}
	}
}
