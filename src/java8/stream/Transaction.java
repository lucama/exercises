package java8.stream;

public class Transaction
{
	private String name;	
	private int year;
	public String getName()
	{
		return name;
	}
	public int getYear()
	{
		return year;
	}
	public int getValue()
	{
		return value;
	}
	private int value;
	public Transaction(String name, int year, int value)
	{
		super();
		this.name = name;
		this.year = year;
		this.value = value;
	}
	@Override
	public String toString()
	{
		return "Transaction [name=" + name + ", year=" + year + ", value=" + value + "]";
	}
	
	
}
