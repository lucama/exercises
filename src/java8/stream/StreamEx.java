package java8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamEx
{
/*
 * 
 * 1)
 * Given two lists of numbers, how would you return all pairs of numbers? 
 * For example, given a list [1, 2, 3] and a list [3, 4] 
 * you should return [(1, 3), (1, 4), (2, 3),(2, 4), (3, 3), (3, 4)].
 * 
 * For simplicity, you can represent a pair as an array with two elements.
 * 
 * 2)
 * Return only pairs whose sum is divisible by 3; for example, (2, 4) and (3, 3) are 
 * valid
 * 
 * 
 * http://what-when-how.com/Tutorial/topic-624i83o/Java-8-in-Action-Lambdas-Streams-and-Functional-Style-Programming-128.htmls
 */
	public static void main(String[] args)
	{
		//1)
		List<Integer> numbers1 = Arrays.asList(1, 2, 3); 
		List<Integer> numbers2 = Arrays.asList(3, 4);
		List<int[]> pairs = numbers1.stream().flatMap(i -> numbers2.stream()
				.map(j -> new int []{i, j})
				).collect(Collectors.toList());
		
		pairs.forEach(x -> System.out.println(Arrays.toString(x)));
		
		//2)
		 System.out.println("-----");
		
		List<int[]> pairs2 = numbers1.stream().flatMap(i -> numbers2.stream()
				.filter(j -> (i + j) % 3 == 0)
				.map(j -> new int[]{i, j}))
				.collect(Collectors.toList());

		pairs2.forEach(x -> System.out.println(Arrays.toString(x)));
	}

}
