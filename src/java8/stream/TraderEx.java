package java8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;


public class TraderEx
{
/*
1) Find all transactions in the year 2011 and sort them
by value (small to high).
2. What are all the unique cities where the traders work?
3. Find all traders from Cambridge and sort them by
name.
4. Return a string of all traders� names sorted
alphabetically.
5. Are any traders based in Milan?
6. Print all transactions� values from the traders living in
Cambridge.
7. What�s the highest value of all the transactions?
8. Find the transaction with the smallest value.
 */
	public static void main(String[] args)
	{
		
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario","Milan");
		Trader alan = new Trader("Alan","Cambridge");
		Trader brian = new Trader("Brian","Cambridge");
		List<Transaction> transactions = Arrays.asList(
		new Transaction("Brian", 2011, 300),
		new Transaction("Raoul", 2012, 1000),
		new Transaction("Raoul", 2011, 400),
		new Transaction("Mario", 2012, 710),
		new Transaction("Mario", 2012, 700),
		new Transaction("Alan", 2012, 950)
		);
		
		List<Trader> traders =  Arrays.asList(raoul,mario,alan,brian);
		
		System.out.println("-- 1 --");
		//1)
		transactions.stream().
			filter(t -> t.getYear()==2011).
			sorted((Transaction t1, Transaction t2)->t1.getValue()-t2.getValue()).
			forEach(System.out::println);
		
		//2
		System.out.println("-- 2 --");
		traders.stream().
		collect(Collectors.toCollection(() -> new TreeSet<Trader>((p1, p2) -> p1.getCity().compareTo(p2.getCity())))).
		forEach(x -> System.out.println(x.getCity())); 
		
		//3
		System.out.println("-- 3 --");
		traders.stream().
		filter(x -> x.getCity().equals("Cambridge")).
		sorted((Trader t1, Trader t2)-> t1.getName().compareTo(t2.getName())).
		forEach(x -> System.out.println(x.getName()));
		
		//4
		System.out.println("-- 4 --");
		String namesList = traders.stream().
		sorted((Trader t1, Trader t2)-> t1.getName().compareTo(t2.getName())).
		map(t -> t.getName()).
	    reduce("", String::concat);

		System.out.println(namesList);
		
		//5
		System.out.println("-- 5 --");
		Optional<Trader> milanTrader = traders.stream().
		filter(t -> t.getCity().equals("Milan")).
		findAny();
		
		System.out.println("Are any traders based in Milan? " +milanTrader.isPresent());
		
		//6
		System.out.println("-- 6 --");
		List<String> cambridgeTraders = traders.stream().
		filter(t -> t.getCity().equals("Cambridge")).
		map(x -> x.getName()).
		collect(toList());
		
		//System.out.println(cambridgeTraders);
		
		transactions.stream().
		filter(x -> cambridgeTraders.contains(x.getName())).
		map(x -> x.getValue()).
		forEach(System.out::println);
		
		
		//7
		System.out.println("-- 7 --");
		System.out.println(transactions.stream().
		max((Transaction t1, Transaction t2)-> t1.getValue() - t2.getValue()).get().getValue());
		
		//8
		System.out.println("-- 8 --");
		System.out.println(
		transactions.stream().
		min((Transaction t1, Transaction t2)-> t1.getValue() - t2.getValue()).get());
	}

}
