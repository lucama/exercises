package java8.microkatas;

import static java.util.Arrays.*;
import java.util.*;

import com.sun.activation.registries.MailcapTokenizer;

import static java.util.stream.Collectors.*;



public class MicroKatas
{

    public static void main(String[] args)
    {

        List<String> collection = asList("My", "name", "is", "John", "Doe");
        List<List<String>> nestedList = asList(asList("Viktor", "Farcic"), asList("John", "Doe", "Third"));
        
        Person sara = new Person("Sara", 4,"Italy");
        Person viktor = new Person("Viktor", 40, "Usa");
        Person eva = new Person("Eva", 42, "Usa");
        List<Person> people = asList(sara, eva, viktor);

        List<Integer> numbers = asList(1, 2, 3, 4, 5);

               

        System.out.println("original collection:" + collection);
        System.out.println("--------------------");
        System.out.println("convert list in upperCase_ Java7:" + listToUppercaseJ7(collection));
        System.out.println("convert list in upperCase_ Java8:" + listToUppercaseJ8(collection));
        System.out.println("--------------------");
        System.out.println("filter list only element with len<4_ Java7:" + filterCollectionJ7(collection));
        System.out.println("filter list only element with len<4_ Java8:" + filterCollectionJ8(collection));
        System.out.println("--------------------");
        System.out.println("original nestedList :" + nestedList);
        System.out.println("flat Map _ Java7:" + FlattenListJ7(nestedList));
        System.out.println("flat Map _ Java8:" + FlattenListJ8(nestedList));
        System.out.println("--------------------");
        System.out.println("original peoplelist :" + people);
        System.out.println("oldest person J7:" + getOldestPersonJ7(people));
        System.out.println("original peoplelist :" + getOldestPersonJ8(people));
        System.out.println("--------------------");
        System.out.println("original number list :" + numbers);
        System.out.println("sum J7:" + calculateSumJ7(numbers));
        System.out.println("sum J8:" + calculateSumJ8(numbers));
        System.out.println("--------------------");
        System.out.println("original people list :" + people);
        System.out.println("get young people J7:" + getYoungPeopleJ7(people));
        System.out.println("get young people J8:" + getYoungPeopleJ8(people));
        System.out.println("--------------------");
        System.out.println("original people list :" + people);
        System.out.println("partitioning young-old J7:" + partitionAdults7(people));
        System.out.println("partitioning young-old J8:" + partitionAdults8(people));
        System.out.println("--------------------");
        System.out.println("original people list :" + people);
        System.out.println("partitioning by Nation j7 :" + groupByNation7(people));
        System.out.println("partitioning by Nation j8:" + groupByNation8(people));
        
    }
    
    //1  Convert elements of a collection to upper case
    //1.a ----- - JAVA7
    public static List<String> listToUppercaseJ7(List<String> collection) {

        List<String> coll = new ArrayList<> ();

        for (String element : collection) {

            coll.add(element.toUpperCase());
        }
        return coll;
    }


    //1.b - JAVA8
    public static List<String> listToUppercaseJ8(List<String> collection) {

        return collection.stream() //trasformo collection in stream
                .map(String::toUpperCase)  // converto ogni elemento in upper case --versione lambda Ex
                // map(String :: toUpperCase).                  // converto ogni elemento in upper case --versione Method ref
                .collect(toList());//trasformo stream in list
    }
    
    //2  elements with less than 4 characters are returned
    //2.a - JAVA7
    public static List<String> filterCollectionJ7(List<String> collection) {

        List<String> newCollection = new ArrayList<>();
        for (String element : collection) {
            if (element.length() < 4) {
                newCollection.add(element);
            }
        }
        return newCollection;
    }
      
       
    //2.b - JAVA8
    public static List<String> filterCollectionJ8(List<String> collection) {

        return collection.stream() // Convert collection to Stream
                .filter(value -> value.length() < 4) // Filter elements with length smaller than 4 characters
                .collect(toList()); // Collect results to a new list
    }

    //3 Flat a nested list
    
    //3.a java 7
    public static List<String> FlattenListJ7(List<List<String>> collection) {

        List<String> newCollection = new ArrayList<>();
        for (List<String> subCollection : collection) {
            for (String value : subCollection) {
                newCollection.add(value);
            }
        }
        return newCollection;
    }
    
    //3.b java 8
    public static List<String> FlattenListJ8(List<List<String>> collection) {
        return collection.stream() // Convert collection to Stream
                .flatMap(value -> value.stream()) // Replace list with stream
                .collect(toList()); // Collect results to a new list
    }

    // 4 get oldest person in people
    //4.a J7
    public static Person getOldestPersonJ7(List<Person> people) {
        Person oldestPerson = new Person("", 0,"");
        for (Person person : people) {
            if (person.getAge() > oldestPerson.getAge()) {
                oldestPerson = person;
            }

        }
        return oldestPerson;
    }

    public static Person getOldestPersonJ8(List<Person> people) {
        Comparator<Person> peopleComparator = (Person o1,Person o2)-> o1.getAge().compareTo(o2.getAge());
    
        return people.stream() // Convert collection to Stream
       // .max(Comparator.comparing(Person::getAge)) // Compares people ages
  
        .max(peopleComparator) // Compares people ages
        .get(); // Gets stream result
    }

    //5 Sum all elements in collection of integer
 

    public static int calculateSumJ7(List<Integer> numbers) {

        int total = 0;
        for (int number : numbers) {
            total += number;
        }
        return total;
    }
         
    public static int calculateSumJ8(List<Integer> people) {

        return people.stream() // Convert collection to Stream
             // Sum elements with 0 as starting value
        // .reduce(0, (a, b) -> a+ b);  //1. generic version using reduce
           .mapToInt(Integer::intValue).sum(); //2. using mapToInt
    }

    
  //6 Filter people under age of 18.
    public static List<Person>  getYoungPeopleJ7(List<Person> people) {
        List<Person> youngPeople = new ArrayList<Person>();
        
        for(Person person:people){
            if(person.getAge()<18){
                youngPeople.add(person);
            }
        }
        return youngPeople;
    }
    
    public static List<String>  getYoungPeopleJ8(List<Person> people) {
            return people.stream() // Convert collection to Stream
                  .filter(person -> person.getAge() < 18)
                  .map(Person::getName)//useful to retry only name in list
                  .collect(toList());
    }
    
    //7 Partition adults and kids.

    public static Map<Boolean, List<Person>> partitionAdults7(List<Person> people) {

        Map<Boolean, List<Person>> map = new HashMap<>(); 
        map.put(true, new ArrayList<>());
        map.put(false, new ArrayList<>());       
        for (Person person : people) {        
            map.get(person.getAge() >= 18).add(person);
        }
        return map;
    }


    public static Map<Boolean, List<Person>> partitionAdults8(List<Person> people) {
        return people.stream() // Convert collection to Stream
      .collect(partitioningBy(p -> p.getAge() >= 18)); // Partition stream of people into adults (age => 18) and kids
      }
         

    //8 Group people by nationality.
    public static Map<String, List<Person>> groupByNation7(List<Person> people) {
            Map<String, List<Person>> map = new HashMap<>();
            for (Person person : people) {
                if (!map.containsKey(person.getNation())){
                    map.put(person.getNation(), new ArrayList<>());
                }
                map.get(person.getNation()).add(person);
            }
            return map;
    }
        
    public static Map<String, List<Person>> groupByNation8(List<Person> people) {
        return people.stream() // Convert collection to Stream
        .collect(groupingBy(Person::getNation)); // Group people by nationality
    }

}
