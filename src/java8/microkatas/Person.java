package java8.microkatas;

public class Person
{

    String name;
    Integer age;
    String nation;
    public Person(String name, Integer age, String nation)
    {
        super();
        this.name = name;
        this.age = age;
        this.nation = nation;
    }
    public String getNation()
    {
        return nation;
    }
    public void setNation(String nation)
    {
        this.nation = nation;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public Integer getAge()
    {
        return age;
    }
    public void setAge(Integer age)
    {
        this.age = age;
    }
   
  
    @Override
    public String toString()
    {
        return (name + "--> "+ age);
    }
}
