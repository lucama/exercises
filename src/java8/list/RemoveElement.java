package java8.list;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class RemoveElement
{

	private static List<Integer> numbers = new ArrayList<>();
	private static final Integer NUM_OF_ELEMENTS = 1000000;
	
	public static void main(String[] args)
	{
		
		populateList();
		
		//java7
		Instant t1 = Instant.now();
		Iterator<Integer> iterator = numbers.iterator();
        while (iterator.hasNext()) {
            Integer number = iterator.next();
            if (number%2 == 0) {
                iterator.remove();
            }
        }
        System.out.println("rimuovo num pari java7:" +Duration.between(t1, Instant.now()).toMillis());
        System.out.println(numbers.size());
        
        populateList();
        
        //java8
		Instant t3 = Instant.now();
		numbers.removeIf(number ->number %2 == 0);
        System.out.println("rimuovo num pari java8:" +Duration.between(t3, Instant.now()).toMillis());
        System.out.println(numbers.size());

	}
	
	private static  void populateList (){
		numbers.clear();
		for(int idx =0 ; idx <NUM_OF_ELEMENTS; idx++){
			numbers.add(idx);
		}
	}

}
