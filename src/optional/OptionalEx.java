package optional;


import java.util.Optional;

public class OptionalEx
{

    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
        String value="a";
        Optional<String> a= Optional.ofNullable(null);
        Optional<String> b=Optional.of(value);
        
        
        System.out.println("First parameter is present: " + a.isPresent());
        System.out.println("Second parameter is present: " + b.isPresent());
          
        
        //Optional.orElse - returns the value if present otherwise returns
        //the default value passed.
        String value1 = a.orElse(new String("c"));
          
        //Optional.get - gets the value, value should be present
        String value2 = b.get();
        System.out.println(a.get()+ value2);
        
        Optional optEmpty = Optional.empty();
        String val = null;
       // optEmpty.ifPresent(val -> print(x));

    }

}
