package openOfficeExamples;



import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
 
public class CalcCreate
{

	public static String OUT_FOLDER = "C:\\Users\\10095067\\Luca\\my\\openofficeex";
	public static String FILE_NAME = "foglio";
	

	
	    public void readODS(File file) {
	        Sheet sheet;
	        Boolean foundHeaderRow = false;
	        Integer nameHeaderColumn = 0;
	        try {
	            // Getting the 0th sheet for manipulation| pass sheet name as string
	            sheet = SpreadSheet.createFromFile(file).getSheet(0);
	 
	            // Get row count and column count
	            int nColCount = sheet.getColumnCount();
	            int nRowCount = sheet.getRowCount();
	 
	            
	            
	            System.out.println("Rows :" + nRowCount);
	            System.out.println("Cols :" + nColCount);
	            // Iterating through each row of the selected sheet
	            MutableCell cell = null;
	            
	            
	            for (int nRowIndex = 0; nRowIndex < nRowCount; nRowIndex++) {
	                // Iterating through each column
	                int nColIndex = 0;
	                for (; nColIndex < nColCount; nColIndex++) {
	                    cell = sheet.getCellAt(nColIndex, nRowIndex);
	                    if (!foundHeaderRow) {
	                        if (cell.getValue().toString().equals("nome")) {
	                            // System.out.println(cell.getValue() + " ");
	                            System.out.print("found header row, index:"
	                                    + nColIndex);
	                            foundHeaderRow = true;
	                            nameHeaderColumn = nColIndex;
	                            break;
	                        }
	                    } else {
	                        if (nColIndex == 1) {
	                            String name = cell.getValue().toString();
	                            if (!name.equals("")) {
	                                System.out.println(name);
	                                String[] split = name.split(" ");
	                                StringBuilder str = new StringBuilder(split[0]
	                                        + "." + split[1] + "@a.b.it");
	                                System.out.println(str);
	                            }
	                        }
	                    }
	 
	                }
	                // System.out.println();
	            }
	 
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    
	    public void createSheet (File file, Month mese) throws FileNotFoundException, IOException {
	    	

	    	
	    	final Object[][] data = new Object[2][2];
	    	data[0] = new Object[] { "", "" };
	    	data[1] = new Object[] { "Progetti e Cliente", "" };
	    	data[1] = new Object[] { "Politecnico di milano", "" };

	    	String[] columnsBase = new String[] { " ", "MESE:" , mese.name()};

	    	TableModel model = new DefaultTableModel(data, columnsBase);
	    	
	    	
	    	Sheet firstSheet = SpreadSheet.createEmpty(model).getFirstSheet();
	    	firstSheet.setRowCount(100);
	    	firstSheet.setColumnCount(100);
	    	
	    	//formattazione intestazione
	    	((MutableCell)firstSheet.getCellAt("B1")).setBackgroundColor(new Color(159,0, 255));
	    	((MutableCell)firstSheet.getCellAt("C1")).setBackgroundColor(new Color(159,0, 255));
	    	((MutableCell)firstSheet.getCellAt("A3")).setBackgroundColor(Color.GRAY);
	    	//((MutableCell)firstSheet.getCellAt("A3")).setBackgroundColor(Color.GRAY);
	    	//((MutableCell)firstSheet.getCellAt("A3")).setBackgroundColor(Color.GRAY);
	    	
	    
	    	String styleName = ((MutableCell<?>)firstSheet.getCellAt("C1")).getStyleName();
	    	
	    	//intestazione mese
	    	//((MutableCell)firstSheet.getCellAt("A1")).setValue("MESE:");
	    	//((MutableCell)sheet.getCellAt("2")).setValue(mese.name());
	    	
//	    	sheet.getCellAt(arg0)CellAt(2, 2).setValue("B2");
	    	
	    	
	    	buildDays(firstSheet, mese);
	    	
	    	firstSheet.getSpreadSheet().saveAs(file);
	    	
	    }
	 
	    public static void main(String[] args) throws FileNotFoundException, IOException {
	      
	        File file = new File(OUT_FOLDER,FILE_NAME);
	 
	        // D:\Maderna\luca\dropbox\Dropbox
	        CalcCreate createCalc = new CalcCreate();
	        createCalc.createSheet(file, Month.AUGUST);

	    }
	    
	    public static void buildDays (Sheet firstSheet, Month mese){
	    	String suffixcell= "3";
	    	char prefixCell = 'D';
	    	boolean afterZ =false;
	    	LocalDate startDate = LocalDate.of(2016, mese, 1);
	    	
	    	LocalDate curDate = startDate;
	    	
	    	while (curDate.getMonth().equals(mese)){
	    		try
				{
	    			String currentCell=null;
	    			
	    			if(!afterZ){
	    				currentCell = String.valueOf(prefixCell)+suffixcell;
	    			}else {
	    				currentCell = "A"+String.valueOf(prefixCell)+suffixcell;
	    			}
				//	String currentCell = String.valueOf(prefixCell)+suffixcell;
					
					//imposto valore
					((MutableCell)firstSheet.getCellAt(currentCell)).setValue(curDate.getDayOfMonth());
					//imposto sfondo
					if((curDate.getDayOfWeek().equals(DayOfWeek.SUNDAY))||
							(curDate.getDayOfWeek().equals(DayOfWeek.SATURDAY))){
						((MutableCell)firstSheet.getCellAt(currentCell)).setBackgroundColor(Color.YELLOW);
					}else {
						((MutableCell)firstSheet.getCellAt(currentCell)).setBackgroundColor(Color.GRAY);
					}
					
					if(prefixCell=='Z'){
						afterZ=true;
						prefixCell='A';
					}else {
						prefixCell++;
					}
					
					
					
					curDate = curDate.plusDays(1);
					System.out.println(curDate);
				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
	    		
	    	}
	    	
	    }
	 
}


