package conc.executors;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ScheduledThreadPoolEx implements Runnable
{
	
	private static final ScheduledExecutorService exec = new ScheduledThreadPoolExecutor(2);
	private static CyclicBarrier barrier = new CyclicBarrier(3);

	public static void main(String[] args) throws InterruptedException, BrokenBarrierException
	{
		
		Instant start = Instant.now();
		System.out.println(Instant.now()+" main thread : " + Thread.currentThread().getName());
		ScheduledThreadPoolEx ex = new ScheduledThreadPoolEx();
		exec.submit(ex);
		exec.submit(ex);
		//barrier.await();
		System.out.println(Instant.now()+" duration: "+Duration.between(start, Instant.now()));
		System.out.println(barrier.getParties());
		barrier.await();
		System.out.println("after await");
		exec.shutdown();
	}

	@Override
	public void run()
	{
		try
		{
			Thread.sleep(500);
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Instant.now()+" exec thread: " + Thread.currentThread().getName());
		try
	{
			System.out.println(barrier.getParties());
			barrier.await();
			System.out.println("after await thr");
		} catch (InterruptedException e)
	{
			// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (BrokenBarrierException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
