package java8tutorial.lambda1exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ArrayEx
{

	public static void main(String[] args)
	{
		List<String> retList = new ArrayList<>();
		String[] cookies={ "A", "B","C","B"}; 

		Optional<String> optional = 
				Arrays.stream(cookies)
				.filter(x -> x.equals("B")).findFirst();
				//.forEach(x -> retList.add(x));

//		if(optional.isPresent()) {//Check whether optional has element you are looking for
//			String p = optional.get();//get it from optional
//			System.out.println("ok");
//		}else {
//			System.out.println("ko");
//		}
//		
				System.out.println(retList);
		

	}
	
	

}
