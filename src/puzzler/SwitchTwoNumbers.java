package puzzler;

public class SwitchTwoNumbers
{
	/* 
	 * http://www.java67.com/2015/08/how-to-swap-two-integers-without-using.html
	 
		How to swap two Integers without using temporary variable in Java?
		Read more: http://www.java67.com/2015/08/how-to-swap-two-integers-without-using.html#ixzz4M6XCBnSW

*/
	public static void main(String[] args)
	{
	
		Integer x =250;
		Integer y =250;
		System.out.println("pre x:"+ x+", y:"+y);
		x = x ^ y;
		y = x ^ y; 
		x = x ^ y; // now x = y
		System.out.println("post x:"+ x+", y:"+y);
		

	}

}
