package google.sheets.example2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.GoogleAuthTokenFactory;
import com.google.gdata.client.Service;
import com.google.gdata.client.http.HttpGDataRequest;
import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.docs.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GClient2
{
	
	static String REDIRECT_URI ="urn:ietf:wg:oauth:2.0:oob";

	static String CLIENT_ID = "46772049238-dj78j47ae28hqb6op5idt2aj3t8t3o7o.apps.googleusercontent.com";
	static String CLIENT_SECRET = "7Ug8TWVM-vCNu-5qaPyXvABN";
	
	
	private static final String SPREADSHEET_SERVICE_URL = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
	
	static List<String> SCOPES = Arrays.asList("https://spreadsheets.google.com/feeds");
	
	public static void main(String[] args) throws IOException, ServiceException
	{
		GClient2 client = new GClient2();
		client.startClient();

	}


	private SpreadsheetService service;
	
	
	private void startClient() throws IOException, ServiceException{
		
		this.service = new SpreadsheetService("myappl");
		Credential credential = getCredentials();
		this.service.setOAuth2Credentials(credential);
		
		com.google.gdata.data.spreadsheet.SpreadsheetEntry spreadsheet = getSpreadsheet("prova");
		List<WorksheetEntry> worksheets = spreadsheet.getWorksheets();
		WorksheetEntry primoFoglio = worksheets.get(0);
		
		
		
		
//per modifica	
		WorksheetFeed worksheetFeed = service.getFeed(
		primoFoglio.getCellFeedUrl(), WorksheetFeed.class);
		
		List<WorksheetEntry> worksheetsToMod = worksheetFeed.getEntries();
		WorksheetEntry primoFoglioMod = worksheets.get(0);

		
		
		
		// Fetch the list feed of the worksheet.
	    URL listFeedUrl = primoFoglioMod.getListFeedUrl();
	    ListFeed listFeed = service.getFeed(listFeedUrl, ListFeed.class);

	    
	    // Write header line into Spreadsheet
	    URL cellFeedUrl= primoFoglioMod.getCellFeedUrl ();
	    CellFeed cellFeed= service.getFeed (cellFeedUrl,CellFeed.class);

	    CellEntry cellEntry= new CellEntry (1, 1, "headline1");

	    cellFeed.insert (cellEntry);
	    cellEntry= new CellEntry (1, 2, "headline2");
	    cellFeed.insert (cellEntry);
	    
	    //UPDATE ROWs
	    // TODO: Choose a row more intelligently based on your app's needs.
	   // ListEntry row = listFeed.getEntries().get(0);

	    // Update the row's data.
	   // row.getCustomElements().setValueLocal("firstname", "Sarah");
	   // row.getCustomElements().setValueLocal("lastname", "Hunt");
	   // row.getCustomElements().setValueLocal("age", "32");
	   // row.getCustomElements().setValueLocal("height", "154");

	    // Save the row using the API.
	//    row.update();
	 //   row = service.insert(listFeedUrl, row);
		
	    
	    // Create a local representation of the new row.
//	    ListEntry row1 = new ListEntry();
////	    row.getCustomElements().setValueLocal("firstname", "Joe");
////	    row.getCustomElements().setValueLocal("lastname", "Smith");
////	    row.getCustomElements().setValueLocal("age", "26");
////	    row.getCustomElements().setValueLocal("height", "176");
//
//	    
//	    Map<String,Object> rowValues = new HashMap<>();
//	    rowValues.put("1", "AAAA");
//	    ListEntry row = createRow(rowValues);
//	    
//	    // Send the new row to the API for insertion.
//	    row = service.insert(listFeedUrl, row);
		
	    
//	    Map<String,Object> rowValues = new HashMap<>();
//	    rowValues.put("A", "AAAA");
//	    ListEntry row = createRow(rowValues);
//	    row = service.insert(listFeedUrl, row);
	    
	  
	}
	
	
	/**
	   * Retrieve OAuth 2.0 credentials.
	   * 
	   * @return OAuth 2.0 Credential instance.
	   */
	  static Credential getCredentials() throws IOException {
	    HttpTransport transport = new NetHttpTransport();
	    JacksonFactory jsonFactory = new JacksonFactory();

	    // Step 1: Authorize -->
	    String authorizationUrl =
	        new GoogleAuthorizationCodeRequestUrl(CLIENT_ID, REDIRECT_URI, SCOPES).build();

	    // Point or redirect your user to the authorizationUrl.
	    System.out.println("Go to the following link in your browser:");
	    System.out.println(authorizationUrl);

	    // Read the authorization code from the standard input stream.
	    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("What is the authorization code?");
	    String code = in.readLine();
	    // End of Step 1 <--

	    // Step 2: Exchange -->
	    GoogleTokenResponse response =
	        new GoogleAuthorizationCodeTokenRequest(transport, jsonFactory, CLIENT_ID, CLIENT_SECRET,
	            code, REDIRECT_URI).execute();
	    // End of Step 2 <--

	    // Build a new GoogleCredential instance and return it.
	    return new GoogleCredential.Builder().setClientSecrets(CLIENT_ID, CLIENT_SECRET)
	        .setJsonFactory(jsonFactory).setTransport(transport).build()
	     .setAccessToken(response.getAccessToken()).setRefreshToken(response.getRefreshToken());
	  }

	  
	  private com.google.gdata.data.spreadsheet.SpreadsheetEntry getSpreadsheet(String sheetName) {
		    try {
		        URL spreadSheetFeedUrl = new URL(SPREADSHEET_SERVICE_URL);

		        SpreadsheetQuery spreadsheetQuery = new SpreadsheetQuery(
		        spreadSheetFeedUrl);
		        spreadsheetQuery.setTitleQuery(sheetName);
		        spreadsheetQuery.setTitleExact(true);
		        SpreadsheetFeed spreadsheet = ((Service) service).getFeed(spreadsheetQuery,
		                                               SpreadsheetFeed.class);

		        if (spreadsheet.getEntries() != null
		                 && spreadsheet.getEntries().size() == 1) {
		            return spreadsheet.getEntries().get(0);
		        } else {
		            return null;
		        }
		    } catch (Exception ex) {
		        ex.printStackTrace();
		    }

		    return null;
		}
	  
	  
	  private ListEntry createRow(Map<String, Object> rowValues) {
		    ListEntry row = new ListEntry();
		    for (String columnName : rowValues.keySet()) {
		        Object value = rowValues.get(columnName);
		        row.getCustomElements().setValueLocal(columnName,
		                         String.valueOf(value));
		    }
		    return row;
		}
	  
}
