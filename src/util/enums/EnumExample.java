package util.enums;

public enum EnumExample
{
   A ("aaa"),
    B ("bbb"),
    C ("ccc");
      
    
    private String enumName;
    
      public String getEnumName()
    {
        return enumName;
    }


    private EnumExample(String enumName){
          this.enumName = enumName;
      }

 
}
