package util;

public class TestBoolean
{

    public static void main(String[] args)
    {
        
        Boolean b = true;

        System.out.println(String.valueOf(b));    // Prints null
        System.out.println(Boolean.toString(b));  // solleva nPE
        
        b = false;

        System.out.println(String.valueOf(b));    // Prints null
        System.out.println(Boolean.toString(b));  // solleva nPE
        
        b = null;

        System.out.println(String.valueOf(b));    // Prints null
        System.out.println(Boolean.toString(b));  // solleva nPE
        

    }

}
