package util.concurrent.utility;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample implements Runnable{

	
	private CyclicBarrier barrier;
	private int duration;

	public CyclicBarrierExample(CyclicBarrier barrier, int i) {
		this.barrier = barrier;
		this.duration = i*1000;
	}

	public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
		
		CyclicBarrier barrier = new CyclicBarrier(3,new AfterBarrierAction());
		
		CyclicBarrierExample action1 = new CyclicBarrierExample(barrier, 1);
		CyclicBarrierExample action2 = new CyclicBarrierExample(barrier, 5);
		Thread thr1 = new Thread(action1,"1");
		Thread thr2 = new Thread(action2,"2");
		thr1.start();
		thr2.start();
		System.out.println(Thread.currentThread().getName() +
				" waiting at barrier 1");
		barrier.await();
		System.out.println("barrier passed");
		
	}

	@Override
	public void run(){

		System.out.println(Thread.currentThread().getName() +
				"  starting");
		try {
			Thread.sleep(duration);


			System.out.println(Thread.currentThread().getName() +
					" waiting at barrier 1");
			this.barrier.await();
			System.out.println(Thread.currentThread().getName() +
					" end");

			

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static class AfterBarrierAction implements Runnable {

		@Override
		public void run() {
			System.out.println(Thread.currentThread().getName() +
					" after barrier action");
			
		}
		
	}

}
