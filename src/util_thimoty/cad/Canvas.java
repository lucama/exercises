package util_thimoty.cad;

import java.util.ArrayList;
import java.util.List;

import util_thimoty.cad.model.Shape;



public class Canvas {

	private List<Shape> shapes = new ArrayList<>();
	
	public void add(Shape shape) {
		shapes.add(shape);		
	}

	public double calcTotalArea() {
		
		double totalArea = 0.0;
		
		for (Shape shape : shapes) {
			totalArea = totalArea + shape.calcArea();			
		}
		
		return totalArea;
		
	}

}
