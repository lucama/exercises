package util_thimoty.cad.model;

public class Point {

	private int x;
	private int y;
	
	// Attribute injection via the constructor
	public Point(int x, int y) {		
		this.x = x;
		this.y = y;		
	}

}
