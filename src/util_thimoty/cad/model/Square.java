package util_thimoty.cad.model;

public class Square extends Shape {

	private double side;
	
	public Square(Point origin, double side) {
		super(origin);
		this.side = side;
	}

	@Override
	public double calcArea() {
		return side*side;
	}

}
