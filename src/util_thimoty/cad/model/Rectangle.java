package util_thimoty.cad.model;

public class Rectangle extends Shape {
	
	private double height;
	private double width;
	
	public Rectangle(Point origin, double width, double height) {
		super(origin);
		this.width = width;
		this.height = height;
	}

	@Override
	public double calcArea() {
		return width*height;
	}

	
	
}
