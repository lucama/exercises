package util_thimoty.cad.model;

public class Circle extends Shape {

	private double radius;
	
	public Circle(Point origin, double radius) {
		super(origin);
		this.radius = radius;
	}

	@Override
	public double calcArea() {
		return radius*radius*Math.PI;
	}

}
