package util_thimoty.cad.model;

public abstract class Shape {
	
	protected Point origin;
	
	public Shape(Point origin) {
		this.origin = origin;
	}
	
	public abstract double calcArea();

}
