package io;

import java.io.File;
import java.io.IOException;

public class TempFile
{

	public static void main(String[] args) throws IOException
	{
		final File temp;

	    temp = File.createTempFile("temp", "");

	    if(!(temp.delete()))
	    {
	        throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
	    }

	    if(!(temp.mkdir()))
	    {
	        throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
	    }

	    
	    System.out.println(System.getProperty("java.io.tmpdir"));
	
	}

	

}
