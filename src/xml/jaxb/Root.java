//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.10.24 alle 11:16:21 AM CEST 
//


package xml.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="element" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="sourceDir" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="major" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="minor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="build" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="class" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "element"
})
@XmlRootElement(name = "root")
public class Root {

    protected List<Root.Element> element;

    /**
     * Gets the value of the element property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the element property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Root.Element }
     * 
     * 
     */
    public List<Root.Element> getElement() {
        if (element == null) {
            element = new ArrayList<Root.Element>();
        }
        return this.element;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="sourceDir" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="major" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="minor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="build" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="class" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "branch",
        "sourceDir",
        "major",
        "minor",
        "build"
    })
    public static class Element {

        @XmlElement(required = true)
        protected String name;
        @XmlElement(required = false)
        protected String branch;
        @XmlElement(required = true)
        protected String sourceDir;
        @XmlElement(required = true)
        protected String major;
        @XmlElement(required = true)
        protected String minor;
        @XmlElement(required = true)
        protected String build;
        @XmlAttribute(name = "class")
        protected String clazz;

        /**
         * Recupera il valore della proprietÓ name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }
        
        /**
         * Recupera il valore della proprietÓ branch .
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBranch() {
            return branch;
        }

        /**
         * Imposta il valore della proprietÓ name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Recupera il valore della proprietÓ sourceDir.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceDir() {
            return sourceDir;
        }

        /**
         * Imposta il valore della proprietÓ sourceDir.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceDir(String value) {
            this.sourceDir = value;
        }

        /**
         * Recupera il valore della proprietÓ major.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMajor() {
            return major;
        }

        /**
         * Imposta il valore della proprietÓ major.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMajor(String value) {
            this.major = value;
        }

        /**
         * Recupera il valore della proprietÓ minor.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMinor() {
            return minor;
        }

        /**
         * Imposta il valore della proprietÓ minor.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMinor(String value) {
            this.minor = value;
        }

        /**
         * Recupera il valore della proprietÓ build.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBuild() {
            return build;
        }

        /**
         * Imposta il valore della proprietÓ build.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBuild(String value) {
            this.build = value;
        }

        /**
         * Recupera il valore della proprietÓ clazz.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClazz() {
            return clazz;
        }

        /**
         * Imposta il valore della proprietÓ clazz.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClazz(String value) {
            this.clazz = value;
        }

    }

}
