package xml.jaxb;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import static  java.util.stream.Collectors.toMap;

import xml.jaxb.Root.Element;

public class JAxbEx
{

	public static void main(String[] args) throws JAXBException
	{
		File file = new File("src/xml/jaxb/dependencies.xml");
		if (!file.exists()){
			System.out.println("not found file");
		}
		JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Root root = (Root) jaxbUnmarshaller.unmarshal(file);
		
		List<Element> elements = root.element;
		
		Map<String, String> nameBranchMap = elements.stream().
		filter(x -> x.getBranch()!=null).
		collect(toMap(Element::getName, Element::getBranch));
		;
		System.out.println("name and branches:"+nameBranchMap);
		
		for(Element depElement : elements){
			if(depElement.branch!=null)
			{
				System.out.println("found branch on element:"+depElement.getName());
			}
		}
	//	System.out.println(customer);

	}

}
