package xml.sax;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxExample extends DefaultHandler
{
	private static String NAME_ELEMENT = "name";
	private static String BRANCH_ELEMENT = "branch";
	
	private boolean isName =false;
	private boolean isBranch =false;
	private String  currentName="";
	private String  currentBranch="";
	private Map<String, String> mapNameBranch = new HashMap<>();

	public static void main(String[] args)
	{
		SaxExample saxExample = new SaxExample();
		saxExample.parseDocument();
		System.out.println(saxExample.mapNameBranch);
	}

	
	private void parseDocument() {

		//get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {

			//get a new instance of parser
			SAXParser sp = spf.newSAXParser();

			//parse the file and also register this class for call backs
			sp.parse("src/xml/sax/dependencies.xml", this);

		}catch(SAXException se) {
			se.printStackTrace();
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch (IOException ie) {
			ie.printStackTrace();
		}
	}



	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
	{
		super.startElement(uri, localName, qName, attributes);
		
		 if(qName.equals(NAME_ELEMENT)) { isName = true; }
	     if(qName.equals(BRANCH_ELEMENT))  { isBranch = true;  }
	}


	@Override
	public void characters(char[] ch, int start, int length) throws SAXException
	{
		super.characters(ch, start, length);
		
		if (isName) {
            currentName= new String(ch, start, length);
        }
        if (isBranch) {
        	currentBranch = new String(ch, start, length);
        }
	}


	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException
	{
		super.endElement(uri, localName, qName);
		if(qName.equals("element")) {
			mapNameBranch.put(currentName,currentBranch);
			currentName="";
			currentBranch="";
        }
		 if(qName.equals("name")) { isName = false; }
	     if(qName.equals("branch"))  { isBranch = false;  }
	}
}
