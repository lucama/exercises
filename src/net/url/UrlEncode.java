package net.url;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import sun.misc.CharacterEncoder;

public class UrlEncode
{

	public static void main(String[] args) throws UnsupportedEncodingException
	{
		
		System.out.println("default encoding: "  +Charset.defaultCharset().name());
		
		
		String param1Before = "hello there";
		String param1After = URLEncoder.encode(param1Before, "UTF-8");
		System.out.println("param1 before:" + param1Before);
		System.out.println("param1 after:" + param1After);

		String param2Before = "good-bye, friend";
		String param2After = URLEncoder.encode(param2Before, "UTF-8");
		System.out.println("param2 before:" + param2Before);
		System.out.println("param2 after:" + param2After);
		
		String param3Before = "a/b/c";
		String param3After_utf = URLEncoder.encode(param3Before, StandardCharsets.UTF_8.name());
		String param3After_def= URLEncoder.encode(param3Before, StandardCharsets.ISO_8859_1.name());
		System.out.println("param2 before:" + param3Before);
		System.out.println("param2 after utf enc " + param3After_utf);
		System.out.println("param2 after def enc:" + param3After_def);
		
		System.out.println("encode: " +param3Before +" in "+StandardCharsets.UTF_8.name());
		String encodeUtf8 = URLEncoder.encode(param3Before, StandardCharsets.UTF_8.name());
		System.out.println("decode use "+StandardCharsets.ISO_8859_1);
		String decodeIso= URLDecoder.decode(encodeUtf8, StandardCharsets.ISO_8859_1.name());
		System.out.println("decode use "+decodeIso);

	}

}
