package files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class FindFileInSubDir
{

	public static void main(String[] args) throws IOException
	{

		
	}

	
	public static Optional<Path> getFileInPath(String rootDir , String fileName){
		
		if((""==rootDir)||(""==fileName)){
			return Optional.empty();
		}
		
		try
		{
			return Files.walk(Paths.get(rootDir))
			.filter(Files::isRegularFile)
      // .filter(x -> x.getFileName().endsWith("xml"))
     // .filter(p -> p.toString().equals("dependencies.xml"))
			 .filter(p -> p.getFileName().endsWith(fileName))
			 .findFirst();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Optional.empty();
		}
       // .forEach(System.out::println);
	//	 .forEach(x -> System.out.println(x.getFileName()));

		
		
	}
}
