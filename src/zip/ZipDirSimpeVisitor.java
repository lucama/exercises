package zip;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipDirSimpeVisitor
{

	     
	  
	 
	    public static void main(String[] args) throws IOException {
	    	// File file = new File("/Users/pankaj/sitemap.xml");
	        //String zipFileName = "/Users/pankaj/sitemap.zip";
	         
	        File dir = new File("C:\\tmp\\9017478492058490946");
	        String zipDirName = "C:\\tmp\\zipFileVisit2.zip";
	         
	        //zipSingleFile(file, zipFileName);
	         
	       
	        ZipDirSimpeVisitor.zipDir(dir, zipDirName);
	    }
	 
	    
	     
	    public static boolean zipDir(File dir, String zipDirName) throws IOException {
	    	FileOutputStream fos = new FileOutputStream(zipDirName);
	    	ZipOutputStream zos = new ZipOutputStream(fos);
	    	
	    	Integer zippedFiles= 0;
	        Files.walkFileTree(Paths.get(dir.getAbsolutePath()), new SimpleFileVisitor<Path>() {
	            
	           
	            
	            @Override
	            public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
	                
	                System.out.println("Zipping "+filePath);
	                File file = filePath.toFile();
	                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
	                ZipEntry ze = new ZipEntry(filePath.toString().substring(dir.getAbsolutePath().length()+1, filePath.toString().length()));
	                zos.putNextEntry(ze);
	                //read the file and write to ZipOutputStream
	                FileInputStream fis = new FileInputStream(file);
	                byte[] buffer = new byte[1024];
	                int len;
	                while ((len = fis.read(buffer)) > 0) {
	                    zos.write(buffer, 0, len);
	                }
	                zos.closeEntry();
	                fis.close();
	                //zippedFiles++;    
	                return FileVisitResult.CONTINUE;
	            }

	            @Override
	            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
	               
	                return FileVisitResult.CONTINUE;
	            }
	        });
	        zos.close();
            fos.close();
            //System.out.println("Zipped  "+ zippedFiles + " files");
            
            ZipFile zipFile = new ZipFile(zipDirName);
            System.out.println("creato file zip con numero di entries: " +zipFile.size());
	       return true;
	    }

	
}
