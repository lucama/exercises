package lambda_perf;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class LambdaIterationPerfExample
{

	public static void main(String[] args)
	{
		List<Integer> bigList = new ArrayList<Integer>();
		Integer i=0;
		for(int idx=0; idx <100000000;idx++){
			bigList.add(i);
		}
		
		Instant pre, after, gap;
		
		pre = Instant.now();	
		for(Integer ele:bigList){
			ele++;
		}
		after = Instant.now();
		System.out.println("test classic:" +ChronoUnit.MILLIS.between(pre,after));
		
		
		pre = Instant.now();	
		Consumer<Integer> addAction = s -> s++;
		bigList.forEach(addAction);
		after = Instant.now();
		System.out.println("test lambda:" +ChronoUnit.MILLIS.between(pre,after));
		
		
	}

}
