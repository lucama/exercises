package stringex;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringPermutation
{
//http://codereview.stackexchange.com/questions/120041/java-8-string-unique-permutations-in-parallel
	
	public static void main(String[] args)
	{
		String str2Permute = "abcdefgeh";
		
		long startTime = System.currentTimeMillis();
		permutations(str2Permute).forEach(System.out::println);
		long totalTime = System.currentTimeMillis() - startTime;
		
		
		startTime = System.currentTimeMillis();
		permutations(str2Permute).parallel().forEach(System.out::println);
		long totalTimeP = System.currentTimeMillis() - startTime;
		System.out.println("serial:" +  totalTime);
		System.out.println("parallel:" +  totalTimeP);
	}

	
	public static Stream<String> permutations(String str) {
	    if (str.isEmpty()) {
	        return Stream.of("");
	    }
	    return IntStream.range(0, str.length())
	                .boxed()
	                .flatMap(i ->
	                  permutations(str.substring(0, i) + str.substring(i+1)).map(t -> str.charAt(i) + t)
	                );
	}
}
