package java8tututorial.mysol.lambda;

import java.util.Arrays;
import java.util.Comparator;

public class LambdaArraysEx
{

    private static String[] labels = {"c", "A", "b", "B", "a"};
    private static String[] labelsLen = {"aaa", "aa", "ccccs", "a", "cdsaaaaaasa"};
    
    public static void main (String[] args){
        System.out.println("ori array:"+Arrays.toString(labels));
        orderArray(true,labels);
        System.out.println("order ascending:"+Arrays.toString(labels));
        orderArray(false,labels);
        System.out.println("order descending:"+Arrays.toString(labels));
        System.out.println("===========");
        
        System.out.println("ori array:"+Arrays.toString(labelsLen));
        orderArrayByLen(true,labelsLen);
        System.out.println("order ascending ByLen:"+Arrays.toString(labelsLen));
        orderArrayByLen(false,labelsLen);
        System.out.println("order descending ByLen:"+Arrays.toString(labelsLen));
        System.out.println("===========");
        
        System.out.println("ori array:"+Arrays.toString(labelsLen));
        orderArrayByFirstChar(true,labelsLen);
        System.out.println("order ascending ByFirstChar:"+Arrays.toString(labelsLen));
     
        
    }
    
    public static String[] orderArrayByLen(Boolean ascending,String[] labels ){
        if (ascending){
            Arrays.sort(labels,(a,b) -> (a.compareTo(b)));
        }else {
            Arrays.sort(labels,(a,b) -> (b.compareTo(a)));
        }
        
        return labels;
    }
    
    public static String[] orderArray(Boolean ascending,String[] labels){
        if (ascending){
            Arrays.sort(labels,(a,b) -> (a.compareTo(b)));
        }else {
            Arrays.sort(labels,(a,b) -> (b.compareTo(a)));
        }
        
        return labels;
    }
   
    public static String[] orderArrayByFirstChar(Boolean ascending,String[] labels){
        if (ascending){
            Arrays.sort(labels,(a,b) -> (a.charAt(0) - b.charAt(0)));
        }else {
            Arrays.sort(labels,(a,b) -> (b.charAt(0) - a.charAt(0)));
        }
        
        return labels;
    }
    
}
